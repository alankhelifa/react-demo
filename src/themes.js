export const THEMES = { DEFAULT: 'default', DARK: 'dark' };

export const breakpoints = {
  xs: 0,
  sm: 768,
  md: 992,
  lg: 1200,
  xl: 1920.1,
};

const themes = {
  default: {
    spacingUnit: 8,
    appBar: {
      appBarHeight: 56,
      appBarBoxShadow: '1px 0px 2px rgba(0, 0, 0, 0.4)',
      zIndex: 10,
    },
    sidebar: {
      sidebarWidth: 56,
      sidebarWidthOpen: 210,
      zIndex: 10,
    },
    dialog: {
      backgroundColor: 'rgba(0, 0, 0, 0.6)',
      boxShadow:
        '0px 11px 15px -7px rgba(0, 0, 0, 0.5), 0px 24px 38px 3px rgba(0, 0, 0, 0.24), 0px 9px 46px 8px rgba(0,0,0,.12)',
      zIndex: 11,
    },
    surface: {
      outlinedBoxShadow: '0px 0px 2px 0 rgba(60,64,67, 0.5)',
      boxShadow: '0 1px 1px 0 rgba(60,64,67,.08), 0 1px 3px 1px rgba(60,64,67,.16)',
      raisedBoxShadow: '0 1px 8px 0 rgba(60,64,67,.18), 0 4px 16px rgba(60,64,67,0.1)',
    },
    colors: {
      statusBar: '#ffccbc',
      primary: '#ffccbc',
      accent: '#795548',
      accentLight: '#a98274',
      accentDark: '#4b2c20',
      text: '#121212',
      textPrimary: '#212121',
      textAccent: '#ffffff',
      backgroundPrimary: '#fafafa',
      backgroundSecondary: '#ffffff',
      backgroundTertiary: '#ffccbc',
      backgroundAccent: '#ededed',
      input: 'rgba(0, 0, 0, 0.08)',
      inputDisabled: 'rgba(0, 0, 0, 0.04)',
    },
  },
  dark: {
    spacingUnit: 8,
    appBar: {
      appBarHeight: 56,
      appBarBoxShadow: '1px 0px 3px rgba(0, 0, 0, 1)',
    },
    sidebar: {
      sidebarWidth: 56,
      sidebarWidthOpen: 210,
      zIndex: 10,
    },
    dialog: {
      backgroundColor: 'rgba(0, 0, 0, 0.6)',
      boxShadow: '3px 5px 13px 0px rgba(0,0,0,0.8)',
      zIndex: 11,
    },
    surface: {
      outlinedBoxShadow: '0px 0px 2px 0 rgba(60,64,67, 0.5)',
      boxShadow: '0 1px 1px 0 rgba(0,0,0,.08), 0 1px 3px 1px rgba(0,0,0,.16)',
      raisedBoxShadow: '0 1px 4px 0 rgba(0,0,0,.8), 0 4px 10px rgba(0,0,0,0.6)',
    },
    colors: {
      statusBar: '#212121',
      primary: '#ef9a9a',
      accent: '#ef9a9a',
      accentLight: '#ffcccb',
      accentDark: '#ba6b6c',
      text: '#ffffff',
      textPrimary: '#bbbbbb',
      textAccent: '#212121',
      backgroundPrimary: '#121212',
      backgroundSecondary: '#323232',
      backgroundTertiary: '#212121',
      backgroundAccent: '#424242',
      input: 'rgba(255, 255, 255, 0.04)',
      inputDisabled: 'rgba(255, 255, 255, 0.08)',
    },
  },
};

export default themes;
