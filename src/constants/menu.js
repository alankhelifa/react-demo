const menu = [
  {
    path: '/home',
    label: 'home',
    icon: 'home',
  },
  {
    path: '/settings',
    label: 'settings',
    icon: 'settings',
  },
];

export default menu;
