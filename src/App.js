import React, { useEffect, useState } from 'react';
import { css } from 'emotion';
import { REDUCERS_TYPES, setStatusBarColor, useAppContext } from './stores/AppProvider';
import {
  AppBar,
  AppBarRouter,
  AppContent,
  IconButton,
  Main,
  Menu,
  Router,
  Sidebar,
} from './components';
import withTheme from './hocs/withTheme';
import menu from './constants/menu';
import Routes from './Routes';
import AppBarRoutes from './AppBarRoutes';

const styles = theme => ({
  width: '100%',
  backgroundColor: theme.colors.backgroundPrimary,
  display: 'flex',
  flexDirection: 'column',
});

function App() {
  const [{ theme, sidebarOpen }, dispatcher] = useAppContext();
  const [open, setOpen] = useState(sidebarOpen);

  useEffect(() => {
    setStatusBarColor(theme.colors.statusBar);
  }, [theme.colors.statusBar]);

  const handleSidebar = () => {
    const value = !open;
    setOpen(value);
    dispatcher({ type: REDUCERS_TYPES.SIDEBAR, value: value });
  };

  return (
    <section id="app-container" className={css(styles(theme))}>
      <AppBar>
        <AppBarRouter>
          <AppBarRoutes>
            <IconButton icon="menu" onClick={handleSidebar} />
          </AppBarRoutes>
        </AppBarRouter>
      </AppBar>
      <AppContent>
        <Sidebar type={'docked'} open={open} extendOnHover={false} handler={handleSidebar}>
          <Menu menu={menu} onClick={handleSidebar} />
        </Sidebar>
        <Main>
          <Router>
            <Routes />
          </Router>
        </Main>
      </AppContent>
    </section>
  );
}

export default withTheme(App);
