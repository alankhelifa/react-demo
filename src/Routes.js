import React, { useState } from 'react';
import * as PropTypes from 'prop-types';
import { Redirect, Route, Switch, withRouter } from 'react-router-dom';
import { Home, NotFound, Settings } from './pages';
import posed, { PoseGroup } from 'react-pose';

const Routes = ({ location, ...props }) => {
  const [Container] = useState(posed.div(props.animate && props.animationConfig));

  return (
    <PoseGroup>
      <Container key={location.pathname.split('/')[1]} style={{ height: '100%' }}>
        <Switch location={location}>
          <Redirect exact from="/" to="/home" />
          <Route exact path="/home" component={Home} key="home" />
          <Route path="/settings" component={Settings} key="settings" />
          <Route component={NotFound} key="not-found" />
        </Switch>
      </Container>
    </PoseGroup>
  );
};

Routes.propTypes = {
  location: PropTypes.object,
};

export default withRouter(Routes);
