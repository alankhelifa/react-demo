import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import en from "./en/";
import fr from "./fr/";

async function initI18n(language) {
  await i18n.use(initReactI18next).init({
    resources: {
      en: { translation: en },
      fr: { translation: fr }
    },
    lng: language,
    interpolation: {
      escapeValue: false
    }
  });
}

export default initI18n;
