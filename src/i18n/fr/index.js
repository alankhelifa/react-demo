export default {
  account: 'Compte',
  accountSettings: 'Paramètres du compte',
  general: 'Général',
  generalSettings: 'Paramètres généraux',
  helloWorld: 'cc tout le monde',
  home: 'Accueil',
  interface: 'Interface',
  toggleLanguage: 'Changer de langue',
  toggleTheme: 'Changer de theme',
  settings: 'Paramètres',
  settingsMainGeneral: 'Interface, langue',
  settingsMainAccount: 'Email, mot de passe, informations',
  useDarkTheme: 'Utiliser le theme sombre',
};
