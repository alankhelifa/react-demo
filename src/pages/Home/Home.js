import React from 'react';
import { css } from 'emotion';
import withTheme from '../../hocs/withTheme';
import { useTranslation } from 'react-i18next';
import { Icon } from '@rmwc/icon';
import {
  Card,
  CardActionButton,
  CardActions,
  CardContent,
  CardMedia,
  Checkbox,
  Grid,
  Radio,
  ScrollView,
  Switch,
  Title,
} from '../../components';

const styles = theme => ({
  padding: theme.spacingUnit * 3,
});

const Home = ({ theme }) => {
  const { t } = useTranslation();
  const [checked, setChecked] = React.useState(false);
  const [value, setValue] = React.useState('Nature');
  const [liked, setLiked] = React.useState(false);
  const [starred, setStarred] = React.useState(false);

  const toggle = () => {
    setChecked(!checked);
  };

  const chooseOption = e => {
    setValue(e.currentTarget.value);
  };

  const like = () => {
    setLiked(!liked);
  };

  const star = () => {
    setStarred(!starred);
  };

  return (
    <ScrollView className={css(styles(theme))}>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <p>{t('helloWorld')}</p>
        </Grid>

        <Grid item container xs={12} spacing={1}>
          <Grid item>
            <Checkbox label="pok" checked={checked} onChange={toggle} />
          </Grid>
          <Grid item>
            <Checkbox label="Chookity" checked={checked} onChange={toggle} />
          </Grid>
          <Grid item>
            <Checkbox label="pok" checked={checked} onChange={toggle} />
          </Grid>
        </Grid>

        <Grid item xs={12}>
          <Switch label="Cookies" checked={checked} onChange={toggle} />
        </Grid>

        <Grid item container xs={12} spacing={1}>
          <Grid item>
            <Radio
              checked={value === 'Caramel'}
              label="Caramel"
              value="Caramel"
              onChange={chooseOption}
            />
          </Grid>
          <Grid item>
            <Radio
              checked={value === 'Chocolate'}
              label="Chocolate"
              value="Chocolate"
              onChange={chooseOption}
            />
          </Grid>
          <Grid item>
            <Radio
              checked={value === 'Nature'}
              label="Nature"
              value="Nature"
              onChange={chooseOption}
            />
          </Grid>
        </Grid>

        <Grid container>
          <Grid item xs={12} md={4} lg={3} xl={2}>
            <Card
              onSwipeRight={like}
              swipeRightIcon="favorite"
              onSwipeLeft={star}
              swipeLeftIcon="star"
              swipeRightColor="#ff3232"
              swipeLeftColor="#ffd700"
            >
              <CardContent>
                <CardMedia
                  height={200}
                  src={
                    liked
                      ? 'https://66.media.tumblr.com/702e57e8a9412b8c828d31758bf50362/tumblr_p4vzlqrzp91x6zqvvo1_400.gif'
                      : 'https://i.imgur.com/COKf8bg.jpg'
                  }
                />
                <div style={{ display: 'flex', alignItems: 'center' }}>
                  <span style={{ width: '100%' }}>
                    <Title type={'h6'}>Mooncake</Title>
                  </span>
                  <span style={{ flexShrink: 0, textAlign: 'right' }}>
                    <Icon
                      icon={starred ? 'star' : 'star_border'}
                      style={starred ? { color: '#ffd700' } : undefined}
                    />
                  </span>
                </div>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                  incididunt ut labore et dolore magna aliqua.
                </p>
              </CardContent>
              <CardActions>
                <CardActionButton>Chookity</CardActionButton>
                <CardActionButton>pok</CardActionButton>
              </CardActions>
            </Card>
          </Grid>
        </Grid>
      </Grid>
    </ScrollView>
  );
};

export default withTheme(Home);
