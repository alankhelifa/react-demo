import React from 'react';
import { css } from 'emotion';
import withTheme from '../../hocs/withTheme';
import { ScrollView } from '../../components';

const styles = theme => ({
  padding: theme.spacingUnit * 3,
});

const NotFound = ({ theme }) => {
  return (
    <ScrollView className={css(styles(theme))}>
      <p>La page que vous cherchez n'existe pas</p>
    </ScrollView>
  );
};

export default withTheme(NotFound);
