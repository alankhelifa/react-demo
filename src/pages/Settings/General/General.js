import React from 'react';
import { css } from 'emotion';
import { useTranslation } from 'react-i18next';
import posed from 'react-pose';
import { REDUCERS_TYPES, useAppContext } from '../../../stores/AppProvider';
import { THEMES } from '../../../themes';
import withTheme from '../../../hocs/withTheme';
import { Grid, Title, Switch, Paper } from '../../../components';
import { HideAt } from 'react-with-breakpoints';

const transition = {
  ease: 'easeOut',
  duration: 400,
};

const GeneralDiv = posed.div({
  init: {
    x: '100%',
    opacity: 0.6,
    transition,
  },
  enter: {
    x: 0,
    opacity: 1,
    transition,
  },
  exit: {
    x: '120%',
    opacity: 0.6,
    transition,
  },
});

const styles = theme => ({
  width: '100%',
  maxWidth: '1200px',
  margin: 'auto',
});

const General = ({ history, theme }) => {
  const [{ appTheme, language }, dispatch] = useAppContext();
  const { t, i18n } = useTranslation();
  const { THEME, LANGUAGE } = REDUCERS_TYPES;

  const handleThemeChange = () => {
    const newTheme = appTheme === THEMES.DEFAULT ? THEMES.DARK : THEMES.DEFAULT;
    dispatch({
      type: THEME,
      value: newTheme,
    });
  };

  const handleLanguageChange = async () => {
    const newLanguage = language === 'en' ? 'fr' : 'en';
    await i18n.changeLanguage(newLanguage);
    dispatch({
      type: LANGUAGE,
      value: newLanguage,
    });
  };

  return (
    <GeneralDiv>
      <div className={css(styles(theme))}>
        <Paper>
          <Grid container spacing={3} justify={'center'}>
            <Grid item container spacing={3}>
              <Grid item xs={12}>
                <Title type={'h6'}>{t('interface')}</Title>
              </Grid>
              <Grid item container xs={12} md={5}>
                <Switch
                  checked={appTheme === THEMES.DARK}
                  label={t('useDarkTheme')}
                  onChange={handleThemeChange}
                  style={{ width: '100%', justifyContent: 'space-between' }}
                />
              </Grid>
              <HideAt breakpoint="mediumAndBelow">
                <Grid item md={2} />
              </HideAt>
              <Grid item xs={12} md={5}>
                <Switch
                  checked={language === 'en'}
                  label={t('toggleLanguage')}
                  onChange={handleLanguageChange}
                  style={{ width: '100%', justifyContent: 'space-between' }}
                />
              </Grid>
            </Grid>
          </Grid>
        </Paper>
      </div>
    </GeneralDiv>
  );
};

export default withTheme(General);
