import React from 'react';
import { css } from 'emotion';
import { useTranslation } from 'react-i18next';
import { Title } from '../../components';
import withTheme from '../../hocs/withTheme';

const SettingsToolbar = ({ theme, children }) => {
  const { t } = useTranslation();
  return (
    <>
      {children}
      <Title type={'h6'} className={css({ margin: `0 ${theme.spacingUnit}px` })}>
        {t('settings')}
      </Title>
    </>
  );
};

export default withTheme(SettingsToolbar);
