import React from 'react';
import * as PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { css, cx } from 'emotion';
import { Icon } from '@rmwc/icon';
import { Card, CardContent } from '../../../components';
import withTheme from '../../../hocs/withTheme';

const styles = theme => ({
  display: 'flex',
  outline: 'none',
  WebkitTapHighlightColor: 'transparent',
  '> .settings-list-item-card': {
    '.settings-list-item-card-content': {
      flexDirection: 'row',
      '> div': {
        flexDirection: 'column',
        '&.content': {
          width: '100%',
        },
        '&.arrow-container': {
          display: 'flex',
          padding: `0 ${theme.spacingUnit * 2}px`,
          flexShrink: 0,
          alignItems: 'center',
          justifyContent: 'center',
          color: theme.colors.accent,
        },
      },
    },
  },
});


const SettingListItemContent = ({ to, className, theme, children, ...props }) => {
  return (
    <NavLink to={to} className={cx(css(styles(theme)), className)} {...props}>
      <Card className="settings-list-item-card" raiseOnHover>
        <CardContent className="settings-list-item-card-content">
          <div className="content">{children}</div>
          <div className="arrow-container">
            <Icon icon="keyboard_arrow_right" />
          </div>
        </CardContent>
      </Card>
    </NavLink>
  );
};

SettingListItemContent.propTypes = {
  to: PropTypes.string.isRequired,
  className: PropTypes.string,
};

export default withTheme(SettingListItemContent);
