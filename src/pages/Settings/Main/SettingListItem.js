import React from 'react';
import { css } from 'emotion';
import withTheme from '../../../hocs/withTheme';

const styles = theme => ({
  '&:not(:first-child)': {
    marginTop: theme.spacingUnit,
  },
  '&:not(:last-child)': {
    marginBottom: theme.spacingUnit,
  },
});

const SettingListItem = ({ theme, children, ...props }) => {
  return (
    <li className={css(styles(theme))} {...props}>
      {children}
    </li>
  );
};

export default withTheme(SettingListItem);
