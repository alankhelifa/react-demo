import React from 'react';
import { css } from 'emotion';
import withTheme from '../../../hocs/withTheme';

const styles = theme => ({
  display: 'flex',
  flexDirection: 'column',
});

const SettingList = ({ theme, children, ...props }) => {
  return (
    <ul className={css(styles(theme))} {...props}>
      {children}
    </ul>
  );
};

export default withTheme(SettingList);
