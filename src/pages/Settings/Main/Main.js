import React from 'react';
import posed from 'react-pose';
import { css } from 'emotion';
import { useTranslation } from 'react-i18next';
import withTheme from '../../../hocs/withTheme';
import { Title } from '../../../components';
import SettingList from './SettingList';
import SettingListItem from './SettingListItem';
import SettingListItemContent from './SettingListItemContent';

const transition = {
  ease: 'easeOut',
  duration: 400,
};

const MainDiv = posed.div({
  init: {
    x: '-100%',
    opacity: 0.6,
    transition,
  },
  enter: {
    x: 0,
    opacity: 1,
    transition,
  },
  exit: {
    x: '-120%',
    opacity: 0.6,
    transition,
  },
});

const styles = theme => ({
  width: '100%',
  maxWidth: '1200px',
  margin: 'auto',
});

const Main = ({ theme }) => {
  const { t } = useTranslation();
  return (
    <MainDiv>
      <div className={css(styles(theme))}>
        <SettingList>
          <SettingListItem>
            <SettingListItemContent to="/settings/general">
              <Title type="h6">{t('general')}</Title>
              <p>{t('settingsMainGeneral')}</p>
            </SettingListItemContent>
          </SettingListItem>
          <SettingListItem>
            <SettingListItemContent to="/settings/account">
              <Title type="h6">{t('account')}</Title>
              <p>{t('settingsMainAccount')}</p>
            </SettingListItemContent>
          </SettingListItem>
        </SettingList>
      </div>
    </MainDiv>
  );
};

export default withTheme(Main);
