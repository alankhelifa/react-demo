import React from 'react';
import { compose } from 'recompose';
import { css } from 'emotion';
import { Redirect, Route, Switch, withRouter } from 'react-router-dom';
import posed, { PoseGroup } from 'react-pose';
import { ScrollView } from '../../components';
import withTheme from '../../hocs/withTheme';
import Main from './Main/Main';
import General from './General/General';
import Account from './Account/Account';

const styles = theme => ({
  padding: theme.spacingUnit * 3,
  button: {
    marginRight: theme.spacingUnit,
  },
});

const Container = posed.div();

const Settings = ({ theme, location }) => {
  return (
    <ScrollView className={css(styles(theme))}>
      <PoseGroup preEnterPose={'init'}>
        <Container key={location.pathname} withParent={false}>
          <Switch location={location}>
            <Route exact path="/settings" component={Main} key="/settings" />
            <Route exact path="/settings/general" component={General} key="/settings/general" />
            <Route exact path="/settings/account" component={Account} key="/settings/account" />
            <Redirect to="/not-found" />
          </Switch>
        </Container>
      </PoseGroup>
    </ScrollView>
  );
};

export default compose(
  withTheme,
  withRouter,
)(Settings);
