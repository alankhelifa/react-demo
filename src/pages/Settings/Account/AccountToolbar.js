import React from 'react';
import { css } from 'emotion';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { IconButton, Title } from '../../../components';
import withTheme from '../../../hocs/withTheme';

const AccountToolbar = ({ theme, history }) => {
  const { t } = useTranslation();
  const handleClick = () => {
    history.goBack();
  };
  return (
    <>
      <IconButton onClick={handleClick} icon={'keyboard_backspace'} />
      <Title type={'h6'} className={css({ margin: `0 ${theme.spacingUnit}px` })}>
        {t('accountSettings')}
      </Title>
    </>
  );
};

export default compose(
  withRouter,
  withTheme,
)(AccountToolbar);
