import React from 'react';
import { css } from 'emotion';
import posed from 'react-pose';
import withTheme from '../../../hocs/withTheme';

const transition = {
  ease: 'easeOut',
  duration: 400,
};

const AccountDiv = posed.div({
  init: {
    x: '100%',
    opacity: 0.6,
    transition,
  },
  enter: {
    x: 0,
    opacity: 1,
    transition,
  },
  exit: {
    x: '120%',
    opacity: 0.6,
    transition,
  },
});

const styles = theme => ({
  width: '100%',
  maxWidth: '1200px',
  margin: 'auto',
});

const Account = ({ theme }) => {
  return (
    <AccountDiv>
      <div className={css(styles(theme))} />
    </AccountDiv>
  );
};

export default withTheme(Account);
