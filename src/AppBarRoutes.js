import React, { useState } from 'react';
import * as PropTypes from 'prop-types';
import { Route, Switch, withRouter } from 'react-router-dom';
import posed, { PoseGroup } from 'react-pose';
import GeneralToolbar from './pages/Settings/General/GeneralToolbar';
import AccountToolbar from './pages/Settings/Account/AccountToolbar';
import SettingsToolbar from './pages/Settings/SettingsToolbar';

const defaultMap = {
  '/home': 1,
};

const AppBarRoutes = ({ location, children, ...props }) => {
  const [Container] = useState(posed.div(props.animate && props.animationConfig));

  const renderSettingsToolbar = props => <SettingsToolbar {...props} children={children} />;

  return (
    <PoseGroup>
      <Container
        key={defaultMap[location.pathname] ? 'default' : location.pathname}
        className={'toolbar'}
      >
        <Switch location={location}>
          <Route exact path="/settings" render={renderSettingsToolbar} key={'/settings'} />
          <Route
            exact
            path="/settings/general"
            component={GeneralToolbar}
            key={'/settings/general'}
          />
          <Route
            exact
            path="/settings/account"
            component={AccountToolbar}
            key={'/settings/account'}
          />
          <Route key={'default'}>{children}</Route>
        </Switch>
      </Container>
    </PoseGroup>
  );
};

AppBarRoutes.propTypes = {
  location: PropTypes.object,
};

export default withRouter(AppBarRoutes);
