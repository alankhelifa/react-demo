import React, { createContext, useContext, useReducer } from 'react';
import themes from '../themes';

export const AppContext = createContext();

export const AppProvider = ({ reducer, initialState, children }) => (
  <AppContext.Provider value={useReducer(reducer, initialState)} children={children} />
);

export const useAppContext = () => useContext(AppContext);

export const setStatusBarColor = color => {
  document.querySelector('meta[name=theme-color]').setAttribute('content', color);
};

const updateTheme = (state, theme) => ({
  ...state,
  appTheme: theme,
  theme: themes[theme],
});

const updateLanguage = (state, language) => {
  return { ...state, language: language };
};

const toggleSidebar = (state, open) => {
  return { ...state, sidebarOpen: open };
};

const REDUCERS = {
  THEME: { key: 'theme', method: updateTheme },
  LANGUAGE: { key: 'language', method: updateLanguage },
  SIDEBAR: { key: 'sidebarOpen', method: toggleSidebar },
};
export const REDUCERS_TYPES = { THEME: 'THEME', LANGUAGE: 'LANGUAGE', SIDEBAR: 'SIDEBAR' };

export const appReducer = (state, action) => {
  const reducer = REDUCERS[action.type];
  if (!reducer) {
    return state;
  }
  localStorage.setItem(reducer.key, JSON.stringify(action.value));
  return reducer.method(state, action.value);
};
