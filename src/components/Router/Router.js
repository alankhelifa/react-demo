import { Children, cloneElement, useEffect } from 'react';
import * as PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';

const Router = ({ animate, location, children, animationConfig }) => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, [location.pathname]);

  if (animate) {
    return cloneElement(Children.only(children), { animate, animationConfig });
  }
  return children;
};

Router.propTypes = {
  animate: PropTypes.bool,
  animationConfig: PropTypes.object,
};

Router.defaultProps = {
  animate: true,
  animationConfig: {
    init: { opacity: 0 },
    enter: { opacity: 1, delay: 300 },
    exit: { opacity: 0 },
  },
};

export default withRouter(Router);
