import React from 'react';
import * as PropTypes from 'prop-types';
import { css, cx } from 'emotion';
import { Button } from '@rmwc/button';
import withTheme from '../../hocs/withTheme';

const style = (theme, { raised, unelevated, outlined }) => ({
  backgroundColor:
    raised || unelevated
      ? `${theme.colors.accent}!important`
      : `transparent!important`,
  color:
    raised || unelevated
      ? `${theme.colors.textAccent}!important`
      : `${theme.colors.accentDark}!important`,
  borderColor: outlined && `${theme.colors.accent}!important`,
  // textTransform: 'none',
  letterSpacing: 'inherit',
  '&::before, &::after': {
    backgroundColor: theme.colors.accentDark,
  },
});

const CustomButton = ({ theme, className, ...props }) => {
  return (
    <Button
      className={cx([css(style(theme, props)), className])}
      {...props}
    />
  );
};

CustomButton.propTypes = {
  children: PropTypes.string.isRequired,
  raised: PropTypes.bool,
  unelevated: PropTypes.bool,
  outlined: PropTypes.bool,
  dense: PropTypes.bool,
  icon: PropTypes.string,
  trailingIcon: PropTypes.element,
  disabled: PropTypes.bool,
  className: PropTypes.string,
  onClick: PropTypes.func,
};

export default withTheme(CustomButton);
