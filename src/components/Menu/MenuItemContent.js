import React from 'react';
import * as PropTypes from 'prop-types';
import { css } from 'emotion';
import { NavLink } from 'react-router-dom';
import withTheme from '../../hocs/withTheme';
import { Icon } from '@rmwc/icon';
import { useTranslation } from 'react-i18next';
import {breakpoints} from "../../themes";

const styles = theme => ({
  display: 'flex',
  height: '100%',
  width: theme.sidebar.sidebarWidthOpen,
  '> .menu-item-icon, > .menu-item-label': {
    display: 'inherit',
    height: 'inherit',
    alignItems: 'center',
  },
  '> .menu-item-icon': {
    width: theme.sidebar.sidebarWidth,
    flexShrink: 0,
    justifyContent: 'center',
  },
  '> .menu-item-label': {
    width: '100%',
    justifyContent: 'flex-start',
  },
});

const MenuItemContent = ({ path, label, icon, onClick, theme }) => {
  const { t } = useTranslation();
  const handleClick = () => {
    if (window.innerWidth <= breakpoints.md) {
      onClick(false);
    }
  };
  return (
    <NavLink to={path} className={css(styles(theme))} onClick={handleClick}>
      <span className="menu-item-icon">
        <Icon icon={icon} />
      </span>
      <span className="menu-item-label">{t(label)}</span>
    </NavLink>
  );
};

MenuItemContent.propTypes = {
  path: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
  onClick: PropTypes.func,
};

export default withTheme(MenuItemContent);
