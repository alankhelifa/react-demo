import React from 'react';
import { Ripple } from '@rmwc/ripple';

const MenuItem = ({ children }) => (
  <Ripple surface>
    <li className={'menu-item'} children={children} />
  </Ripple>
);

export default MenuItem;
