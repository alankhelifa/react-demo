import React from 'react';
import * as PropTypes from 'prop-types';
import { css, cx } from 'emotion';
import withTheme from '../../hocs/withTheme';
import MenuItem from './MenuItem';
import MenuItemContent from './MenuItemContent';

const styles = theme => ({
  height: '100%',
  width: '100%',
  overflow: 'hidden',
  '> .menu-item': {
    position: 'relative',
    height: 48,
    width: theme.sidebar.sidebarWidthOpen,
    color: theme.colors.textPrimary,
    ':hover': {
      backgroundColor: theme.colors.backgroundAccent,
      color: theme.colors.text,
    },
    '> a': {
      outline: 'none',
    },
    '> a.active': {
      color: theme.colors.accent,
    },
    '> a:after': {
      content: "' '",
      display: 'block',
      position: 'absolute',
      left: 0,
      height: '100%',
      width: 0,
      backgroundColor: theme.colors.accent,
      transition: 'width ease-in 0.2s',
    },
    '> a.active:after': {
      width: 4,
    },
  },
});

const Menu = ({ menu, onClick, className, theme }) => {
  return (
    <ul className={`${cx(css(styles(theme)), className)} menu`}>
      {menu.map(item => (
        <MenuItem key={item.label}>
          <MenuItemContent {...item} onClick={onClick} />
        </MenuItem>
      ))}
    </ul>
  );
};

Menu.defaultProps = {
  onClick: () => {},
};

Menu.propTypes = {
  menu: PropTypes.arrayOf(
    PropTypes.shape({
      path: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired,
      icon: PropTypes.string,
    }),
  ).isRequired,
  className: PropTypes.string,
  onClick: PropTypes.func,
};

export default withTheme(Menu);
