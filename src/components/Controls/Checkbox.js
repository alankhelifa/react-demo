import React from 'react';
import * as PropTypes from 'prop-types';
import { css, cx, keyframes } from 'emotion';
import { Checkbox } from '@rmwc/checkbox';
import withTheme from '../../hocs/withTheme';

const customKeyframesOut = theme => ({
  '0%, 80%': {
    borderColor: theme.colors.accent,
    backgroundColor: theme.colors.accent,
  },
  '100%': {
    borderColor: theme.colors.textPrimary,
    backgroundColor: 'transparent',
  },
});

const customKeyframesIn = theme => ({
  '0%': {
    borderColor: theme.colors.textPrimary,
    backgroundColor: 'transparent',
  },
  '50%': {
    borderColor: theme.colors.accent,
    backgroundColor: theme.colors.accent,
  },
});

const styles = theme => ({
  color: theme.colors.text,
  '.mdc-checkbox .mdc-checkbox__native-control:checked ~ .mdc-checkbox__background::before, .mdc-checkbox .mdc-checkbox__native-control:indeterminate ~ .mdc-checkbox__background::before': {
    backgroundColor: theme.colors.accent,
  },
  '.mdc-checkbox.mdc-checkbox--selected::before, .mdc-checkbox.mdc-checkbox--selected::after': {
    backgroundColor: theme.colors.accent,
  },
  '.mdc-checkbox.mdc-ripple-upgraded--background-focused.mdc-checkbox--selected::before, .mdc-checkbox.mdc-ripple-upgraded--background-focused.mdc-checkbox--selected::after': {
    backgroundColor: theme.colors.accent,
  },
  '.mdc-checkbox__native-control:enabled:not(:checked):not(:indeterminate) ~ .mdc-checkbox__background': {
    borderColor: theme.colors.textPrimary,
  },
  '.mdc-checkbox__native-control:enabled:checked ~ .mdc-checkbox__background, .mdc-checkbox__native-control:enabled:indeterminate ~ .mdc-checkbox__background': {
    borderColor: theme.colors.accent,
    backgroundColor: theme.colors.accent,
  },
  '.mdc-checkbox--anim-unchecked-checked .mdc-checkbox__native-control:enabled ~ .mdc-checkbox__background, .mdc-checkbox--anim-unchecked-indeterminate .mdc-checkbox__native-control:enabled ~ .mdc-checkbox__background': {
    animationName: keyframes(customKeyframesIn(theme)),
  },
  '.mdc-checkbox--anim-checked-unchecked .mdc-checkbox__native-control:enabled ~ .mdc-checkbox__background, .mdc-checkbox--anim-indeterminate-unchecked .mdc-checkbox__native-control:enabled ~ .mdc-checkbox__background': {
    animationName: keyframes(customKeyframesOut(theme)),
  },
});

const CustomCheckbox = ({ className, theme, ...props }) => {
  return <Checkbox className={cx(css(styles(theme)), className)} {...props} />;
};

CustomCheckbox.propTypes = {
  checked: PropTypes.oneOf([true, false]),
  className: PropTypes.string,
  disabled: PropTypes.oneOf([true, false]),
  id: PropTypes.string,
  indeterminate: PropTypes.oneOf([true, false]),
  inputRef: PropTypes.func,
  label: PropTypes.node,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

export default withTheme(CustomCheckbox);
