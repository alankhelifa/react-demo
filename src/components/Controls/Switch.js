import React from 'react';
import * as PropTypes from 'prop-types';
import { css, cx } from 'emotion';
import { Switch } from '@rmwc/switch';
import withTheme from '../../hocs/withTheme';

const styles = theme => ({
  direction: 'rtl',
  color: theme.colors.text,
  '> label': {
    paddingLeft: 0,
    paddingRight: theme.spacingUnit,
  },
  '> .mdc-switch': {
    '.mdc-switch__thumb-underlay::before, .mdc-switch__thumb-underlay::after': {
      backgroundColor: theme.colors.accent,
    },
    '.mdc-switch__thumb, .mdc-switch__track': {
      backgroundColor: theme.colors.textPrimary,
      borderColor: theme.colors.textPrimary,
    },
  },
  '> .mdc-switch--checked': {
    '.mdc-switch__thumb, .mdc-switch__track': {
      backgroundColor: theme.colors.accent,
      borderColor: theme.colors.accent,
    },
  },
});

const CustomSwitch = ({ className, theme, ...props }) => {
  return <Switch className={cx(css(styles(theme)), className)} {...props} />;
};

CustomSwitch.propTypes = {
  checked: PropTypes.oneOf([true, false]),
  className: PropTypes.string,
  disabled: PropTypes.oneOf([true, false]),
  id: PropTypes.string,
  indeterminate: PropTypes.oneOf([true, false]),
  inputRef: PropTypes.func,
  label: PropTypes.node,
  onChange: PropTypes.func,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

export default withTheme(CustomSwitch);
