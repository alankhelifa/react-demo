import React from 'react';
import * as PropTypes from 'prop-types';
import { css, cx } from 'emotion';
import { Radio } from '@rmwc/radio';
import withTheme from '../../hocs/withTheme';

const styles = theme => ({
  color: theme.colors.text,
  '.mdc-radio': {
    '&::before, &::after': {
      backgroundColor: theme.colors.accent,
    },
    '.mdc-radio__background::before': {
      backgroundColor: theme.colors.accent,
    },
    '.mdc-radio__native-control:enabled:not(:checked) + .mdc-radio__background .mdc-radio__outer-circle': {
      borderColor: theme.colors.textPrimary,
    },
    '.mdc-radio__native-control:enabled:checked + .mdc-radio__background .mdc-radio__outer-circle, .mdc-radio__native-control:enabled + .mdc-radio__background .mdc-radio__inner-circle': {
      borderColor: theme.colors.accent,
    },
  },
});

const CustomRadio = ({ className, theme, ...props }) => {
  return <Radio className={cx(css(styles(theme)), className)} {...props} />;
};

CustomRadio.propTypes = {
  checked: PropTypes.oneOf([true, false]),
  className: PropTypes.string,
  disabled: PropTypes.oneOf([true, false]),
  id: PropTypes.string,
  indeterminate: PropTypes.oneOf([true, false]),
  inputRef: PropTypes.func,
  label: PropTypes.node,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

export default withTheme(CustomRadio);
