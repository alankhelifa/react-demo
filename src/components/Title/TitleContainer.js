import React from 'react';
import * as PropTypes from 'prop-types';
import { css, cx } from 'emotion';
import withTheme from '../../hocs/withTheme';

const typeStyles = theme => ({
  h1: {
    title: {
      fontSize: '4rem',
      lineHeight: '4rem',
    },
    icon: {
      width: 84,
      height: 84,
      fontSize: '4rem',
    },
    marginBottom: theme.spacingUnit * 4,
  },
  h2: {
    title: {
      fontSize: '3rem',
      lineHeight: '3rem',
    },
    icon: {
      width: 64,
      height: 64,
      fontSize: '3rem',
    },
    marginBottom: theme.spacingUnit * 4,
  },
  h3: {
    title: {
      fontSize: '2.5rem',
      lineHeight: '2.5rem',
    },
    icon: {
      width: 56,
      height: 56,
      fontSize: '2.5rem',
    },
    marginBottom: theme.spacingUnit * 3,
  },
  h4: {
    title: {
      fontSize: '2rem',
      lineHeight: '2rem',
    },
    icon: {
      width: 48,
      height: 48,
      fontSize: '2rem',
    },
    marginBottom: theme.spacingUnit * 3,
  },
  h5: {
    title: {
      fontSize: '1.8rem',
      lineHeight: '1.8rem',
    },
    icon: {
      width: 48,
      height: 48,
      fontSize: '1.8rem',
    },
    marginBottom: theme.spacingUnit * 2,
  },
  h6: {
    title: {
      fontSize: '1.4rem',
      lineHeight: '1.4rem',
    },
    icon: {
      width: 48,
      height: 48,
      fontSize: '1.4rem',
    },
    marginBottom: theme.spacingUnit,
  },
});

const styles = (theme, type) => {
  const specificStyle = typeStyles(theme)[type];
  return {
    display: 'flex',
    marginBottom: specificStyle.marginBottom,
    '> .icon-container': {
      display: 'inline-flex',
      flexShrink: 0,
      alignItems: 'center',
      '> button': {
        ...specificStyle.icon,
      },
    },
    '> .title': {
      display: 'flex',
      alignItems: 'center',
    },
    [`> .title, > .title > ${type}`]: {
      width: '100%',
      ...specificStyle.title,
    },
  };
};

const TitleContainer = ({ type, className, theme, children, ...props }) => {
  return <div className={cx(css(styles(theme, type)), className)} {...props} children={children} />;
};

TitleContainer.propTypes = {
  type: PropTypes.oneOf(['h1', 'h2', 'h3', 'h4', 'h5', 'h6']).isRequired,
  className: PropTypes.string,
};

export default withTheme(TitleContainer);
