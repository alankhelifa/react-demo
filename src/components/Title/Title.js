import React from 'react';
import * as PropTypes from 'prop-types';
import IconButton from '../IconButton/IconButton';
import TitleContainer from './TitleContainer';

const typeMap = {
  h1: 'h1',
  h2: 'h2',
  h3: 'h3',
  h4: 'h4',
  h5: 'h5',
  h6: 'h6',
};

const Title = ({ icon, onIconClick, type, className, children, ...props }) => {
  const Component = typeMap[type] || 'h1';
  return (
    <TitleContainer type={Component} className={className} {...props}>
      {icon && (
        <div className="icon-container">
          <IconButton icon={icon} onClick={onIconClick} />
        </div>
      )}
      <div className="title">
        <Component>{children}</Component>
      </div>
    </TitleContainer>
  );
};

Title.propTypes = {
  children: PropTypes.node.isRequired,
  icon: PropTypes.string,
  onIconClick: PropTypes.func,
  type: PropTypes.oneOf(['h1', 'h2', 'h3', 'h4', 'h5', 'h6']),
  className: PropTypes.string,
};

export default Title;
