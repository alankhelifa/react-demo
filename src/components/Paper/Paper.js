import React from 'react';
import * as PropTypes from 'prop-types';
import { css, cx } from 'emotion';
import withTheme from '../../hocs/withTheme';

const styles = (theme, raised) => ({
  display: 'flex',
  flexWrap: 'wrap',
  overflow: 'hidden',
  width: '100%',
  padding: theme.spacingUnit * 3,
  backgroundColor: theme.colors.backgroundSecondary,
  color: theme.colors.text,
  boxShadow: raised ? theme.surface.raisedBoxShadow : theme.surface.boxShadow,
  borderRadius: 4,
});

const Paper = ({ className, raised, theme, ...props }) => {
  return <div className={cx(css(styles(theme, raised)), className)} {...props} />;
};

Paper.defaultProps = {
  raised: false,
};

Paper.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  raised: PropTypes.bool,
};

export default withTheme(Paper);
