import React from 'react';
import * as PropTypes from 'prop-types';
import { css, cx } from 'emotion';
import SimpleBar from 'simplebar-react';
import 'simplebar/dist/simplebar.min.css';
import withTheme from '../../hocs/withTheme';

const styles = theme => ({
  height: '100%',
  '.simplebar-scrollbar': {
    width: theme.spacingUnit / 2,
    '::before': {
      backgroundColor: theme.colors.accentDark,
      width: theme.spacingUnit / 2,
    },
  },
});

const ScrollView = ({ className, theme, children }) => {
  return <SimpleBar className={cx(css(styles(theme)), className)} children={children} />;
};

ScrollView.propTypes = {
  className: PropTypes.string,
};

export default withTheme(ScrollView);
