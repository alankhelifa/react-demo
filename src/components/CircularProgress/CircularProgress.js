import React from 'react';
import { css, cx } from 'emotion';
import { CircularProgress } from '@rmwc/circular-progress';
import withTheme from '../../hocs/withTheme';

const styles = theme => ({
  color: theme.colors.accent,
});

const CustomCircularProgress = ({ className, theme, ...props }) => {
  return <CircularProgress className={cx(css(styles(theme)), className)} {...props} />;
};

export default withTheme(CustomCircularProgress);
