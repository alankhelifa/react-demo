import React from 'react';
import * as PropTypes from 'prop-types';
import { css, cx } from 'emotion';
import withTheme from '../../hocs/withTheme';

const styles = theme => ({
  position: 'relative',
  display: 'flex',
  height: `${theme.appBar.appBarHeight}px`,
  width: '100%',
  overflow: 'hidden',
  backgroundColor: theme.colors.backgroundTertiary,
  color: theme.colors.textPrimary,
  boxShadow: theme.appBar.appBarBoxShadow,
  zIndex: theme.appBar.zIndex,
  '.toolbar': {
    display: 'inline-flex',
    flex: '1 1 auto',
    overflow: 'hidden',
    padding: `0 ${theme.spacingUnit}px`,
    alignItems: 'center',
  }
});

const AppBar = ({ className, children, theme }) => {
  return (
    <header id="app-bar" className={`${cx(css(styles(theme)), className)}`} children={children} />
  );
};

AppBar.propTypes = {
  className: PropTypes.string,
};

export default withTheme(AppBar);
