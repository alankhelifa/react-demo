import { Children, cloneElement } from 'react';
import * as PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';

const AppBarRouter = ({ animate, location, children, animationConfig }) => {
  if (animate) {
    return cloneElement(Children.only(children), { animate, animationConfig });
  }
  return children;
};

AppBarRouter.propTypes = {
  animate: PropTypes.bool,
  animationConfig: PropTypes.object,
};

AppBarRouter.defaultProps = {
  animate: true,
  animationConfig: {
    init: { opacity: 0 },
    enter: { opacity: 1, delay: 100 },
    exit: { opacity: 0 },
  },
};

export default withRouter(AppBarRouter);
