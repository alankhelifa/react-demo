import React from 'react';
import * as PropTypes from 'prop-types';
import { css } from 'emotion';

const Toolbar = ({ className, children }) => {
  return <div className={`${css(className)} toolbar`} children={children} />;
};

Toolbar.propTypes = {
  className: PropTypes.string,
};

export default Toolbar;
