import React from 'react';
import { IconButton } from '@rmwc/icon-button';
import { css, cx } from 'emotion';
import withTheme from '../../hocs/withTheme';
import * as PropTypes from 'prop-types';

const styles = theme => ({
  display: 'inline-flex',
  width: 40,
  height: 40,
  padding: theme.spacingUnit,
  color: theme.colors.accent,
  '&::before, &::after': {
    backgroundColor: theme.colors.accentDark,
  },
});

const CustomIconButton = ({ className, theme, ...props }) => {
  return <IconButton className={cx(css(styles(theme)), className)} {...props} />;
};

CustomIconButton.propTypes = {
  checked: PropTypes.bool,
  className: PropTypes.string,
  disabled: PropTypes.bool,
  icon: PropTypes.string,
  onChange: PropTypes.func,
  onClick: PropTypes.func,
  onIcon: PropTypes.string,
};

export default withTheme(CustomIconButton);
