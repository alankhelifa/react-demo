import React from 'react';
import PropTypes from 'prop-types';
import { css, cx } from 'emotion';
import withTheme from '../../hocs/withTheme';

const styles = theme => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacingUnit,
  justifyContent: 'flex-end',
  flex: '0 0 auto',
  '> * + *': {
    marginLeft: theme.spacingUnit,
  },
});

const DialogActions = ({ className, theme, ...props }) => {
  return <div className={cx(css(styles(theme)), className)} {...props} />;
};

DialogActions.propTypes = {
  className: PropTypes.string,
};

export default withTheme(DialogActions);
