import React from 'react';
import * as PropTypes from 'prop-types';
import { compose } from 'recompose';
import withPortal from '../../hocs/withPortal';
import withTheme from '../../hocs/withTheme';
import { PoseGroup } from 'react-pose';
import Backdrop from './Backdrop';
import DialogContainer from './DialogContainer';
import DialogTitle from './DialogTitle';

const Dialog = ({ id, title, open, onClose, backdropProps, children, ...props }) => {
  return (
    <PoseGroup animateOnMount>
      {open && [
        <Backdrop onClick={onClose} key={`${id}-dialog-backdrop`} {...backdropProps}>
          <DialogContainer key={`${id}-dialog-container`} {...props}>
            {title && <DialogTitle>{title}</DialogTitle>}
            {children}
          </DialogContainer>
        </Backdrop>,
      ]}
    </PoseGroup>
  );
};

Dialog.propTypes = {
  id: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired,
  className: PropTypes.string,
  onClose: PropTypes.func,
  backdropProps: PropTypes.shape({
    className: PropTypes.string,
  }),
  title: PropTypes.string,
};

export default compose(
  withPortal('dialog-root'),
  withTheme,
)(Dialog);
