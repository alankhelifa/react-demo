import React from 'react';
import { css, cx } from 'emotion';
import posed from 'react-pose';
import withTheme from '../../hocs/withTheme';
import { breakpoints } from '../../themes';

const Container = posed.div({
  init: {
    opacity: 0,
    y: 20,
  },
  enter: {
    opacity: 1,
    y: 0,
    delay: 150,
  },
  exit: {
    opacity: 0,
    y: 20,
    delay: 100,
  },
});

const styles = theme => ({
  display: 'flex',
  flexWrap: 'wrap',
  flexDirection: 'column',
  minWidth: 300,
  maxWidth: 600,
  minHeight: 120,
  backgroundColor: theme.colors.backgroundPrimary,
  color: theme.colors.textPrimary,
  boxShadow: theme.dialog.boxShadow,
  borderRadius: 4,
  zIndex: theme.dialog.zIndex + 1,
  [`@media (min-width: ${breakpoints.md}px)`]: {
    maxWidth: 800,
  },
});

const DialogContainer = ({ className, theme, ...props }) => {
  const stopPropagation = e => {
    e.stopPropagation();
  };
  return (
    <Container onClick={stopPropagation} className={cx(css(styles(theme)), className)} {...props} />
  );
};

export default withTheme(DialogContainer);
