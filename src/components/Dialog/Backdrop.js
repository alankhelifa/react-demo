import React from 'react';
import * as PropTypes from 'prop-types';
import { css, cx } from 'emotion';
import posed from 'react-pose';
import withTheme from '../../hocs/withTheme';

const styles = theme => ({
  position: 'fixed',
  left: 0,
  top: 0,
  right: 0,
  bottom: 0,
  backgroundColor: theme.dialog.backgroundColor,
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  zIndex: theme.dialog.zIndex,
});

const Container = posed.div({
  init: {
    opacity: 0,
  },
  enter: {
    opacity: 1,
    transition: { duration: 200 },
  },
  exit: {
    opacity: 0,
  },
});

const Backdrop = ({ className, theme, ...props }) => {
  return <Container withParent={false} className={cx(css(styles(theme)), className)} {...props} />;
};

Backdrop.propTypes = {
  className: PropTypes.string,
};

export default withTheme(Backdrop);
