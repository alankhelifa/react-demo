import React from 'react';
import * as PropTypes from 'prop-types';
import { css, cx } from 'emotion';
import withTheme from '../../hocs/withTheme';

const styles = theme => ({
  padding: `${theme.spacingUnit}px ${theme.spacingUnit * 3}px`,
});

const DialogContent = ({ className, theme, ...props }) => {
  return <div className={cx(css(styles(theme)), className)} {...props} />;
};

DialogContent.propTypes = {
  className: PropTypes.string,
};

export default withTheme(DialogContent);
