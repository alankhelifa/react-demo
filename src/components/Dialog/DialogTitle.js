import React from 'react';
import { css } from 'emotion';
import withTheme from '../../hocs/withTheme';

const styles = theme => ({
  display: 'flex',
  width: '100%',
  padding: `${theme.spacingUnit * 2}px ${theme.spacingUnit * 3}px`,
  fontWeight: 'bold',
  fontSize: '1.2em',
});

const DialogTitle = ({ theme, children }) => {
  return <div className={css(styles(theme))}>{children}</div>;
};

export default withTheme(DialogTitle);
