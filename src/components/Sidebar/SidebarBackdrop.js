import React from 'react';
import { css } from 'emotion';
import posed from 'react-pose';
import withTheme from '../../hocs/withTheme';
import { breakpoints } from '../../themes';

const styles = theme => ({
  position: 'absolute',
  height: '100%',
  width: '100%',
  zIndex: theme.sidebar.zIndex - 1,
  backgroundColor: 'black',
  opacity: 0,
  top: 0,
  [`@media (max-width: ${breakpoints.md}px)`]: {
    zIndex: theme.sidebar.zIndex,
  },
});

const Container = posed.div({
  enter: {
    opacity: 0.6,
    duration: 150,
  },
  exit: {
    opacity: 0,
    duration: 150,
  },
});

const SidebarBackdrop = ({ theme, ...props }) => {
  return <Container className={css(styles(theme))} {...props} />;
};

export default withTheme(SidebarBackdrop);
