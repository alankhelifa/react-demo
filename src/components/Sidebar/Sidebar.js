import React, { useEffect, useState } from 'react';
import * as PropTypes from 'prop-types';
import { css, cx } from 'emotion';
import { PoseGroup } from 'react-pose';
import withTheme from '../../hocs/withTheme';
import { breakpoints } from '../../themes';
import ScrollView from '../ScrollView/ScrollView';
import SidebarBackdrop from './SidebarBackdrop';

const styles = (theme, open, type, extendOnHover) => ({
  position: type === 'docked' ? 'static' : 'fixed',
  display: 'flex',
  flexDirection: 'column',
  overflow: 'hidden',
  width: open ? theme.sidebar.sidebarWidthOpen : theme.sidebar.sidebarWidth,
  height: '100%',
  backgroundColor: theme.colors.backgroundSecondary,
  color: theme.colors.text,
  boxShadow: '1px 1px 1px rgba(0, 0, 0, 0.1)',
  transition: 'width ease-out 0.25s 0.2s, left ease 0.45s',
  zIndex: theme.sidebar.zIndex,
  flexShrink: 0,
  [`@media (max-width: ${breakpoints.md}px)`]: {
    position: 'fixed',
    width: theme.sidebar.sidebarWidthOpen,
    left: !open ? '-100%' : 0,
    top: 0,
    zIndex: theme.sidebar.zIndex + 1,
  },
  ':hover': {
    width: extendOnHover
      ? theme.sidebar.sidebarWidthOpen
      : open
      ? theme.sidebar.sidebarWidthOpen
      : theme.sidebar.sidebarWidth,
  },
  '> nav': {
    height: '100%',
  },
});

const Sidebar = ({ open, type, extendOnHover, handler, className, theme, children }) => {
  const [windowWidth, setWindowWidth] = useState(window.innerWidth);
  const resize = () => {
    setWindowWidth(window.innerWidth);
  };
  const handleBackdropClick = () => {
    handler(false);
  };
  useEffect(() => {
    window.addEventListener('resize', resize);
    return () => {
      window.removeEventListener('resize', resize);
    };
  }, []);
  return (
    <PoseGroup>
      <aside
        id="sidebar"
        className={`${cx(css(styles(theme, open, type, extendOnHover)), className)}`}
        key="sidebar"
      >
        <nav>
          <ScrollView children={children} />
        </nav>
      </aside>
      {((open && windowWidth <= breakpoints.md && type === 'docked') ||
        (open && type === 'floating')) && (
        <SidebarBackdrop id="sidebar-backdrop" onClick={handleBackdropClick} key="backdrop" />
      )}
    </PoseGroup>
  );
};

Sidebar.propTypes = {
  open: PropTypes.bool,
  type: PropTypes.oneOf(['docked', 'floating']),
  extendOnHover: PropTypes.bool,
  handler: PropTypes.func,
  className: PropTypes.string,
};

Sidebar.defaultProps = {
  type: 'floating',
  extendOnHover: true,
  handler: () => {},
};

export default withTheme(Sidebar);
