import React from 'react';
import { cx, css } from 'emotion';
import { TextField } from '@rmwc/textfield';
import withTheme from '../../hocs/withTheme';

const styles = (theme, disabled) => ({
  backgroundColor: disabled && `${theme.colors.inputDisabled}!important`,
  '&:not(.mdc-text-field--disabled)': {
    backgroundColor: theme.colors.input,
  },
  '&:not(.mdc-text-field--disabled) .mdc-floating-label': {
    color: theme.colors.textPrimary,
  },
  '&:focus-within:not(.mdc-text-field--disabled) .mdc-floating-label': {
    color: theme.colors.accent,
  },
  '&:not(.mdc-text-field--disabled):not(.mdc-text-field--outlined):not(.mdc-text-field--textarea) .mdc-text-field__input, &:not(.mdc-text-field--disabled):not(.mdc-text-field--outlined):not(.mdc-text-field--textarea) .mdc-text-field__input:hover': {
    borderColor: theme.colors.textPrimary,
  },
  '> .mdc-text-field__input': {
    caretColor: theme.colors.accent,
  },
  '> .mdc-line-ripple': {
    backgroundColor: theme.colors.accent,
  },
});

const CustomTextField = ({ className, field, disabled, theme, ...props }) => {
  return <TextField className={cx(css(styles(theme, disabled)), className)} disabled={disabled} {...field} {...props} />;
};

export default withTheme(CustomTextField);
