import React, { useEffect, useState } from 'react';
import * as PropTypes from 'prop-types';
import { css } from 'emotion';
import posed from 'react-pose';
import withTheme from '../../hocs/withTheme';
import CardContainer from './CardContainer';
import { value } from 'popmotion';

export const SWIPE_LEFT_LIMIT = -100;
export const SWIPE_RIGHT_LIMIT = 100;

const styles = theme => ({
  position: 'relative',
  overflow: 'hidden',
  zIndex: 2,
  backgroundColor: theme.colors.backgroundSecondary,
  minWidth: '100px',
});

const Card = ({ onSwipeLeft, onSwipeRight, theme, children, ...props }) => {
  const [startPosition, setStartPosition] = useState(0);
  const [endPosition, setEndPosition] = useState(0);
  let x = value(0);
  const [CardDiv] = useState(
    posed.div({
      pressable: true,
      draggable: onSwipeLeft || onSwipeRight ? 'x' : false,
      dragBounds: {
        left: onSwipeLeft ? SWIPE_LEFT_LIMIT : 0,
        right: onSwipeRight ? SWIPE_RIGHT_LIMIT : 0,
      },
      dragEnd: {
        x: 0,
        transition: {
          ease: 'easeOut',
        },
      },
    }),
  );

  useEffect(() => {
    if (startPosition && endPosition) {
      const swipeDistance = endPosition - startPosition;
      if (swipeDistance <= SWIPE_LEFT_LIMIT) {
        onSwipeLeft && onSwipeLeft();
      }
      if (swipeDistance >= SWIPE_RIGHT_LIMIT) {
        onSwipeRight && onSwipeRight();
      }
      setStartPosition(0);
      setEndPosition(0);
    }
  }, [startPosition, endPosition, onSwipeLeft, onSwipeRight]);

  if (!children) {
    return null;
  }

  const handleDragStart = e => {
    if (e.type === 'mousedown') {
      setStartPosition(e.pageX);
    }
    if (e.type === 'touchstart') {
      setStartPosition(e.targetTouches[0].pageX);
    }
    e.stopPropagation();
  };

  const handleDragEnd = e => {
    if (e.type === 'mouseup') {
      setEndPosition(e.pageX);
    }
    if (e.type === 'touchend') {
      setEndPosition(e.changedTouches[0].pageX);
    }
    e.stopPropagation();
  };

  const valuesMap = { x: x };

  return (
    <CardContainer animatedValues={valuesMap} {...props}>
      <CardDiv
        onPressStart={handleDragStart}
        onPressEnd={handleDragEnd}
        className={css(styles(theme))}
        values={valuesMap}
      >
        {children}
      </CardDiv>
    </CardContainer>
  );
};

Card.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  onSwipeLeft: PropTypes.func,
  onSwipeRight: PropTypes.func,
  swipeLeftIcon: PropTypes.string,
  swipeRightIcon: PropTypes.string,
  swipeLeftColor: PropTypes.string,
  swipeRightColor: PropTypes.string,
  outlined: PropTypes.bool,
  raiseOnHover: PropTypes.bool,
};

export default withTheme(Card);
