import React from 'react';
import * as PropTypes from 'prop-types';
import { css, cx } from 'emotion';
import withTheme from '../../hocs/withTheme';
import CardContainerIcon from './CardContainerIcon';
import CardContainerBackdrop from './CardContainerBackdrop';

const styles = (theme, outlined, raiseOnHover) => ({
  position: 'relative',
  width: '100%',
  overflow: 'hidden',
  borderRadius: 4,
  boxShadow: outlined ? theme.surface.outlinedBoxShadow : theme.surface.boxShadow,
  transition: 'box-shadow 200ms ease-out',
  ':focus, :focus-within, :hover': {
    boxShadow: raiseOnHover
      ? theme.surface.raisedBoxShadow
      : outlined
      ? theme.surface.outlinedBoxShadow
      : theme.surface.boxShadow,
  },
});

const CardContainer = ({
  raiseOnHover,
  outlined,
  swipeLeftIcon,
  swipeRightIcon,
  className,
  animatedValues,
  swipeLeftColor,
  swipeRightColor,
  theme,
  children,
  ...props
}) => {
  return (
    <div className={cx(css(styles(theme, outlined, raiseOnHover)), className)} {...props}>
      <CardContainerBackdrop
        swipeLeftColor={swipeLeftColor}
        swipeRightColor={swipeRightColor}
        animatedValues={animatedValues}
      >
        <CardContainerIcon icon={swipeRightIcon} animatedValues={animatedValues} />
        <CardContainerIcon icon={swipeLeftIcon} animatedValues={animatedValues} />
      </CardContainerBackdrop>
      {children}
    </div>
  );
};

CardContainer.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  swipeLeftIcon: PropTypes.string,
  swipeRightIcon: PropTypes.string,
  outlined: PropTypes.bool,
  raiseOnHover: PropTypes.bool,
};

export default withTheme(CardContainer);
