import React from 'react';
import * as PropTypes from 'prop-types';
import { css, cx } from 'emotion';

const styles = {
  position: 'relative',
  backgroundSize: 'cover',
  backgroundRepeat: 'no-repeat',
  backgroundPosition: 'center',
  objectFit: 'cover',
};

const CardMedia = ({ src, height, className, ...props }) => {
  return (
    <div
      style={{ backgroundImage: `url("${src}")`, height: height || 80 }}
      className={cx(css(styles), className, 'card-media')}
      {...props}
    />
  );
};

CardMedia.propTypes = {
  src: PropTypes.string.isRequired,
  height: PropTypes.number,
  children: PropTypes.node,
  className: PropTypes.string,
};

export default CardMedia;
