import React from 'react';
import PropTypes from 'prop-types';
import { css, cx } from 'emotion';
import withTheme from '../../hocs/withTheme';

export const styles = theme => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacingUnit,
  '& > * + *': {
    marginLeft: theme.spacingUnit,
  },
});

const CardActions = ({ className, theme, ...props }) => {
  return <div className={cx(css(styles(theme)), className)} {...props} />;
};

CardActions.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
};

export default withTheme(CardActions);
