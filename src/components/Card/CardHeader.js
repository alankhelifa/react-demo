import React from 'react';
import * as PropTypes from 'prop-types';
import { css, cx } from 'emotion';
import withTheme from '../../hocs/withTheme';

const styles = theme => ({
  padding: theme.spacingUnit * 2,
});

const CardContent = ({ className, theme, ...props }) => {
  return <div className={cx(css(styles(theme)), className)} {...props} />;
};

CardContent.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
};

export default withTheme(CardContent);
