import React from 'react';
import * as PropTypes from 'prop-types';
import { css, cx } from 'emotion';
import { Ripple } from '@rmwc/ripple';
import withTheme from '../../hocs/withTheme';

const styles = (theme, clickable) => ({
  display: 'flex',
  flexDirection: 'column',
  padding: theme.spacingUnit * 2,
  ':hover': {
    cursor: clickable ? 'pointer' : 'auto',
  },
  'p': {
    fontSize: '0.9rem',
    color: theme.colors.textPrimary,
  },
  '> .card-media': {
    width: `calc(100% + ${theme.spacingUnit * 4}px)`,
    margin: -theme.spacingUnit * 2,
    marginBottom: theme.spacingUnit * 2,
  },
});

const CardContent = ({ className, onClick, ripple, theme, ...props }) => {
  if (ripple) {
    return (
      <Ripple surface>
        <div
          className={cx(css(styles(theme, !!onClick)), className)}
          onClick={onClick}
          {...props}
        />
      </Ripple>
    );
  }
  return (
    <div className={cx(css(styles(theme, !!onClick)), className)} onClick={onClick} {...props} />
  );
};

CardContent.defaultProps = {
  ripple: true,
};

CardContent.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  onClick: PropTypes.func,
  ripple: PropTypes.bool,
};

export default withTheme(CardContent);
