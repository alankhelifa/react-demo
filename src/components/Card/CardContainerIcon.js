import React, { useState } from 'react';
import { css } from 'emotion';
import { Icon } from '@rmwc/icon';
import posed from 'react-pose';
import { transform } from 'popmotion';
import withTheme from '../../hocs/withTheme';
import { SWIPE_LEFT_LIMIT, SWIPE_RIGHT_LIMIT } from './Card';

const { interpolate } = transform;

const styles = theme => ({
  margin: (SWIPE_RIGHT_LIMIT - 24) / 2,
  color: theme.colors.textAccent,
});

const CardContainerIcon = ({ icon, animatedValues, theme }) => {
  const [Container] = useState(
    posed.span({
      passive: {
        scale: ['x', interpolate([SWIPE_LEFT_LIMIT, 0, SWIPE_RIGHT_LIMIT], [1.5, 0.5, 1.5]), true],
      },
    }),
  );
  return (
    <Container parentValues={animatedValues}>
      <Icon icon={icon} className={css(styles(theme))} />
    </Container>
  );
};

export default withTheme(CardContainerIcon);
