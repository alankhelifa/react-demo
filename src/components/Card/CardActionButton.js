import React from 'react';
import { css, cx } from 'emotion';
import Button from '../Button/Button';

const styles = {
  minWidth: 'auto',
};

const CardActionButton = ({ className, ...props }) => {
  return <Button className={cx(css(styles), className)} {...props} />;
};

export default CardActionButton;
