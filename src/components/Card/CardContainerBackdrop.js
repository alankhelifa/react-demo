import React, { useState } from 'react';
import { css } from 'emotion';
import posed from 'react-pose';
import { transform } from 'popmotion';
import withTheme from '../../hocs/withTheme';
import { SWIPE_LEFT_LIMIT, SWIPE_RIGHT_LIMIT } from './Card';

const { interpolate } = transform;

const styles = theme => ({
  position: 'absolute',
  display: 'flex',
  width: '100%',
  height: '100%',
  alignItems: 'center',
  justifyContent: 'space-between',
  zIndex: 1,
  backgroundColor: theme.colors.backgroundSecondary,
  borderRadius: 4,
});

const CardContainerBackdrop = ({
  swipeLeftColor,
  swipeRightColor,
  animatedValues,
  theme,
  children,
}) => {
  const [Container] = useState(
    posed.span({
      passive: {
        backgroundColor: [
          'x',
          interpolate(
            [SWIPE_LEFT_LIMIT, 0, SWIPE_RIGHT_LIMIT],
            [
              swipeLeftColor || theme.colors.accentLight,
              theme.colors.backgroundSecondary,
              swipeRightColor || theme.colors.accentDark,
            ],
            ['easeOut'],
          ),
          true,
        ],
      },
    }),
  );
  return (
    <Container className={css(styles(theme))} children={children} parentValues={animatedValues} />
  );
};

export default withTheme(CardContainerBackdrop);
