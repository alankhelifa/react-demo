import React from 'react';
import * as PropTypes from 'prop-types';
import { css, cx } from 'emotion';
import withTheme from '../../hocs/withTheme';
import { breakpoints } from '../../themes';

const styles = theme => ({
  position: 'relative',
  display: 'flex',
  width: '100%',
  height: '100%',
  overflow: 'hidden',
  [`@media (max-width: ${breakpoints.md}px)`]: {
    position: 'static',
  }
});

const AppContent = ({ className, theme, children }) => {
  return <div id="app-content" className={cx(css(styles(theme), className))} children={children} />;
};

AppContent.propTypes = {
  className: PropTypes.string,
};

export default withTheme(AppContent);
