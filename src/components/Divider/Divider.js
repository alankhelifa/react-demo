import React from 'react';
import { css, cx } from 'emotion';
import withTheme from '../../hocs/withTheme';

const styles = theme => ({
  backgroundColor: theme.colors.primary,
  height: 1,
  border: 'none',
  marginTop: theme.spacingUnit * 2,
  marginBottom: theme.spacingUnit * 2,
});

const Divider = ({ className, theme, ...props }) => {
  return <hr className={cx(css(styles(theme)), className)} {...props} />;
};

export default withTheme(Divider);
