import React from 'react';
import * as PropTypes from 'prop-types';
import { css, cx } from 'emotion';
import withTheme from '../../hocs/withTheme';
import { breakpoints } from '../../themes';

const styles = (theme, withSidebar) => ({
  height: '100%',
  width: '100%',
  padding: withSidebar ? `0 0 0 ${theme.sidebar.sidebarWidth}px` : undefined,
  backgroundColor: theme.colors.backgroundPrimary,
  color: theme.colors.text,
  [`@media (max-width: ${breakpoints.md}px)`]: {
    padding: 0,
  },
});

const Main = ({ withSidebar, className, theme, children }) => {
  return (
    <main
      id="app-main"
      className={`${cx(css(styles(theme, withSidebar)), className)}`}
      children={children}
    />
  );
};

Main.defaultProps = {
  withSidebar: false,
};

Main.propTypes = {
  withSidebar: PropTypes.bool,
  className: PropTypes.string,
};

export default withTheme(Main);
