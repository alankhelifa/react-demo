import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import * as serviceWorker from './serviceWorker';
import '@material/ripple/dist/mdc.ripple.css';
import '@material/button/dist/mdc.button.css';
import '@material/icon-button/dist/mdc.icon-button.css';
import '@material/switch/dist/mdc.switch.css';
import '@material/radio/dist/mdc.radio.css';
import '@material/checkbox/dist/mdc.checkbox.css';
import '@material/form-field/dist/mdc.form-field.css';
import '@material/textfield/dist/mdc.textfield.css';
import '@material/floating-label/dist/mdc.floating-label.css';
import '@material/notched-outline/dist/mdc.notched-outline.css';
import '@material/line-ripple/dist/mdc.line-ripple.css';
import '@rmwc/circular-progress/circular-progress.css';
import { AppProvider, appReducer } from './stores/AppProvider';
import { BreakpointsProvider } from 'react-with-breakpoints';
import initI18n from './i18n/i18n';
import App from './App';
import themes, { THEMES, breakpoints } from './themes';

const _breakpoints = {
  small: breakpoints.sm,
  medium: breakpoints.md,
  large: breakpoints.large,
  xlarge: breakpoints.xl,
};

const initialState = () => {
  let config = {};
  try {
    config.appTheme = JSON.parse(localStorage.getItem('theme')) || THEMES.DEFAULT;
    config.language = JSON.parse(localStorage.getItem('language')) || 'en';
    config.sidebarOpen = JSON.parse(localStorage.getItem('sidebarOpen')) || false;
  } catch (e) {
    config = {
      appTheme: THEMES.DEFAULT,
      language: 'en',
      sidebarOpen: false,
    };
  }
  initI18n(config.language);
  return {
    appTheme: config.appTheme,
    theme: themes[config.appTheme] || themes[THEMES.DEFAULT],
    language: config.language,
    sidebarOpen: config.sidebarOpen,
  };
};

ReactDOM.render(
  <AppProvider initialState={initialState()} reducer={appReducer}>
    <BrowserRouter>
      <BreakpointsProvider breakpoints={_breakpoints}>
        <App />
      </BreakpointsProvider>
    </BrowserRouter>
  </AppProvider>,
  document.getElementById('root'),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
