import React, { useEffect } from 'react';
import ReactDOM from 'react-dom';

const wrappers = {};

const withPortal = targetId => Component => props => {
  const portalId = props.id;
  const target = document.getElementById(targetId);
  useEffect(() => {
    wrappers[portalId] = document.createElement('div');
    target.appendChild(wrappers[portalId]);
    return () => {
      target.removeChild(wrappers[portalId]);
    };
  }, [portalId, target]);
  if (wrappers[portalId] === undefined) {
    return null;
  }
  return ReactDOM.createPortal(<Component {...props} />, wrappers[portalId]);
};

export default withPortal;
