import React from "react";
import { useAppContext } from "../stores/AppProvider";

const withTheme = Component => props => {
  const [{ theme }] = useAppContext();
  return <Component theme={theme} {...props} />;
};

export default withTheme;
